#!/usr/bin/python3

import sys
import musicbrainzngs as mb

def better_print(o, indent=0, prefix=''):
    if isinstance(o, list):
        better_print('[', indent, prefix)
        for index, value in enumerate(o):
            better_print(value, indent+1, str(index) + ' : ')
        better_print(']', indent)
    elif isinstance(o, dict):
        better_print('{', indent, prefix)
        for key, value in o.items():
            better_print(value, indent+1, str(key) + ' -> ')
        better_print('}', indent)
    else:
        print('    '*indent + str(prefix) + str(o))

class Artist:
    def __init__(self):
        self.mbid = None
        self.name = None
        self.albums = []

    def print(self):
        print(str(self.name))
        for album in self.albums:
            album.print()

class Album:
    def __init__(self):
        self.mbid = None
        self.name = None
        self.tracks = []

    def print(self):
        print('    ' + str(self.name))
        for track in self.tracks:
            track.print()

class Track:
    def __init__(self):
        self.mbid = None
        self.name = None

    def print(self):
        print('        ' + str(self.name))

if __name__ == '__main__':
    mb.set_useragent('autodisco', '2')

    try:
        artist_query = ' '.join(sys.argv[1:])
        print('fetching artists for', artist_query)
        artist_dict = mb.search_artists(artist=artist_query)['artist-list'][0]
        artist = Artist()
        artist.mbid = artist_dict['id']
        artist.name = artist_dict['name']
        print('fetching subunits for', artist_dict['name'])
        subunits_dicts = [d['artist'] for d in mb.get_artist_by_id(artist_dict['id'], includes=['artist-rels'])['artist']['artist-relation-list'] if d['type'] == 'subgroup']
        for d in [artist_dict] + subunits_dicts:
            print('fetching albums for', d['name'])
            for release_group_dict in mb.browse_release_groups(artist=d['id'], includes=['release-group-rels', 'artist-credits'])['release-group-list']:
                # do not include album if it is a big collaboration
                if len(release_group_dict['artist-credit']) > 3:
                    print('rejecting (to many artist credits)', release_group_dict['title'])
                    continue
                # do not include album if it is a soundtrack or remix or live
                if 'secondary-type-list' in release_group_dict:
                    if not {'Soundtrack', 'Remix', 'Live'}.isdisjoint(release_group_dict['secondary-type-list']):
                        print('rejecting (remix, live, or soundtrack)', release_group_dict['title'])
                        continue
                # do not include album if it is a single from another album
                if 'release_group-relation-list' in release_group_dict:
                    is_part_of_another_album = False
                    for d in release_group_dict['release_group-relation-list']:
                        if d['type'] == 'single from' and d['direction'] == 'forward':
                            is_part_of_another_album = True
                            break
                    if is_part_of_another_album:
                        print('rejecting (single from antother album)', release_group_dict['title'])
                        continue
                print('fetching releases for', release_group_dict['title'])
                release_list = mb.browse_releases(release_group=release_group_dict['id'])['release-list']
                # only accepts official releases
                release_list = [d for d in release_list if d['status'] == 'Official']
                if len(release_list) == 0:
                    print('rejecting (no official release)', release_group_dict['title'])
                    continue
                # select an appropriate release
                release_dict = release_list[0]
                for d in release_list:
                    # if 'text-representation' in rd and rd['text-representation']['language'] == 'eng':
                    # language is sometime inacurate
                    if d['country'] in ('XW', 'US'):
                        release_dict = d
                        break
                # create album
                album = Album()
                album.mbid = release_dict['id']
                album.name = release_dict['title']
                print('fetching tracks for', release_dict['title'])
                for track_dict in mb.get_release_by_id(release_dict['id'], includes=['recordings'])['release']['medium-list'][0]['track-list']:
                    track = Track()
                    track.mbid = track_dict['id']
                    track.name = track_dict.get('title', track_dict['recording']['title'])
                    album.tracks.append(track)
                artist.albums.append(album)
    except Exception as e:
        print('Error :', e)

    artist.print()
