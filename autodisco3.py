#!/usr/bin/python3

'''
the gui shows the musicbrainz data and whether it is downloaded or not

TODO:cleanup
'''

import sys

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject, Gtk

import musicbrainzngs as mb
mb.set_useragent('autodisco', '2')

treestore = Gtk.TreeStore(
    str, # 0 id
    str, # 1 name
    str, # 2 type
    str, # 3 language
    str, # 4 country
    str, # 5 track count
    )

# empty child so that the expander is visible
def treestore_append(parent, 
    id,
    name, 
    type=None, 
    language=None, 
    country=None,
    trackcount=None,
    add_empty_child=False,
    ):
    treeiter = treestore.append(parent, [
        id, 
        name, 
        type, 
        language, 
        country,
        trackcount,
    ])
    if add_empty_child:
        treestore.append(treeiter, [
            None, 
            None, 
            None, 
            None, 
            None,
            None,
        ])
    return treeiter

artist_query = ' '.join(sys.argv[1:])
artist_dict = mb.search_artists(artist=artist_query)['artist-list'][0]
releasegroups_list = [] # releasegroup_dict
for releasegroup_dict in mb.browse_release_groups(limit=100, artist=artist_dict['id'])['release-group-list']:
    recordings_dict = {} # recoding_id -> recording_dict
    for release_dict in mb.browse_releases(limit=100, release_group=releasegroup_dict['id'], includes=['recordings'])['release-list']:
        for medium_dict in release_dict['medium-list']:
            for track_dict in medium_dict['track-list']:
                recording_dict = track_dict['recording']
                if not ('video' in recording_dict and recording_dict['video']):
                    recordings_dict[recording_dict['id']] = recording_dict
    releasegroup_dict['recordings_dict'] = recordings_dict
    releasegroups_list.append(releasegroup_dict)
# remove subsets (single that are part of another album ...)
for releasegroup_dict in releasegroups_list.copy():
    for i, other_releasegroup_dict in enumerate(releasegroups_list.copy()):
        if not (other_releasegroup_dict is releasegroup_dict) and (set(other_releasegroup_dict['recordings_dict']) <= set(releasegroup_dict['recordings_dict'])):
            del releasegroups_list[i]
# put everithing in the treestore
artist_treeiter = treestore_append(None, artist_dict['id'], artist_dict['name'])
for releasegroup_dict in releasegroups_list:
    releasegroup_type = releasegroup_dict['primary-type']
    if 'secondary-type-list' in releasegroup_dict:
        for t in releasegroup_dict['secondary-type-list']:
            releasegroup_type += ' + ' + t
    releasegroup_treeiter = treestore_append(artist_treeiter, releasegroup_dict['id'], releasegroup_dict['title'], type=releasegroup_type)
    for recording_dict in releasegroup_dict['recordings_dict'].values():
        recording_title = recording_dict['title']
        if 'disambiguation' in recording_dict:
            recording_title += ' (' + recording_dict['disambiguation'] + ')'
        treestore_append(releasegroup_treeiter, recording_dict['id'], recording_title)

treeview = Gtk.TreeView(model=treestore)
treeview.append_column(Gtk.TreeViewColumn('Entry', Gtk.CellRendererText(), text=1))
treeview.append_column(Gtk.TreeViewColumn('Type', Gtk.CellRendererText(), text=2))
treeview.append_column(Gtk.TreeViewColumn('Language', Gtk.CellRendererText(), text=3))
treeview.append_column(Gtk.TreeViewColumn('Country', Gtk.CellRendererText(), text=4))
treeview.append_column(Gtk.TreeViewColumn('Track Count', Gtk.CellRendererText(), text=5))
treeview.get_column(0).set_expand(True)

scrolledwindow = Gtk.ScrolledWindow()
scrolledwindow.add(treeview)

window = Gtk.Window( title='Hello World', default_width=800, default_height=600)
window.connect('destroy', Gtk.main_quit)
window.add(scrolledwindow)
window.show_all()

Gtk.main()
