### Description:
Uses Wikipedia and YoutubeDL to automatically download all music from an artist

### Features:
- Create a simple directory structure
- Check duration to find correct video
- Download album and artist covers
- Add metadata to audio files
- Only download singles if they are not part of an album
- Write a title.missing file if it was unable to donwload a song

### Improvements needed ?
- Better handle different editions of an album (currently download the first one one the page)
- Download from websites with better quality than Youtube (Spotify) ?
